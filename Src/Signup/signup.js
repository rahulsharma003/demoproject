import React, { Component } from 'react';
import  { View,Modal , StyleSheet , TouchableOpacity , ImageBackground,Dimensions,Text ,TextInput}  from 'react-native';
import { Input,Segment,Button,Icon} from 'native-base';
import LinearGradient from 'react-native-linear-gradient';
const deviceHeight=Dimensions.get('window').height;
const deviceWidth=Dimensions.get('window').width;
import Snackbar from'react-native-snackbar';


 import styles from './styles';

export default class Signup extends Component
{
    constructor(props)
    {
        super(props);
        this.state=
        {
          email:'',
          username:"",
          password:"",
          confirm_password:"",
           showPassword: true,
        }
    }

    toggleSwitch() {
       this.setState({ showPassword: !this.state.showPassword });
     }


  toSignin()
  {
     this.props.navigation.navigate('Login');
      //Alert.alert(this.state.dateSelected);
  }


    //Validation on Email
    validateEmail = (email) =>
     {
         var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
         return re.test(email);
     };

  signUp()
    {

           if(this.state.email==='')
          { this._showMessage('Please enter your email address'); }

          else if (!this.validateEmail(this.state.email))
          { this._showMessage('Please enter valid email address'); }

          else if(this.state.username==='')
          { this._showMessage('Please enter user name '); }

          else if(this.state.password==='')
          { this._showMessage('Password cannot be blank'); }

          else if(this.state.confirm_password==='')
          { this._showMessage('Please enter password again'); }

          else if(this.state.password!=this.state.confirm_password)
          { this._showMessage('Password must be same'); }

          else
          {

            console.log(this.state.email);
            console.log(this.state.username);
            console.log(this.state.password);
            console.log(this.state.confirm_password);

            let formdata = new FormData();

            formdata.append("email", this.state.email)
            formdata.append("uname", this.state.username)
            formdata.append("type", 1)
            formdata.append("password", this.state.password)
                fetch('http://itechgaints.com/api-v4/v1/createuser',  {
            method: 'POST',
            //mode:"no-cors",
             headers: {
             'Accept': 'application/json',
            'Content-Type': 'multipart/form-data',
            },
             body: formdata
        })
        .then(response => response.json())
        .then(response =>
          {
            console.log(response);
             // this._showMessage(response.status);
            if(response.status===1)
            {
               this._showMessage(response.message);
            }
            else
            {
              this._showMessage('Email exist');

                }
              }).catch((error) =>{
                  this._showMessage("signup error");

                  console.log(error);
                });
            }


  }

  verifyNumber=(text)=>
  {
    console.log(text);

    var Value = text.length.toString() ;
    this.setState({Length : Value}) ;
    this.setState({phone_number:text});
  }
  verifyValue()
  {
    if(this.state.Length<10)
      {
        this._showMessage1('Phone Number must be 10 Digits Long ');
      }
  }
  _showMessage(message)
  {
     Snackbar.show({
       title: message,
       duration: Snackbar.LENGTH_LONG,
       backgroundColor:'#E44C4F'
     });
  }

  confirmPassword()
  {
     if(this.state.password!=this.state.confirm_password)
       { this._showMessage('Password must be same'); }
  }

  render()
   {
     return(
        <View style={styles.mainview} >

        <View style = {{width:deviceWidth}} >

          <Text style={styles.signup_text}>
          Sign Up
          </Text>

          <TextInput
              style={styles.label}
                 onChangeText={(text)=>this.setState({email:text})}
                    placeholder="Email "
                    placeholderTextColor="#bcbcbc"

                    autoCapitalize="none"
                  />

                  <TextInput
                     onChangeText={(text)=>this.setState({username:text})}
                      style={styles.label}
                            placeholder="Username"
                            placeholderTextColor="#bcbcbc"
                            autoCapitalize="none"
                          />


<View style={styles.row_view}>
<TextInput
   onChangeText={(text)=>this.setState({password:text})}
  style={styles.flexPadding}
  placeholder="Password"
  placeholderTextColor="#bcbcbc"
  autoCapitalize="none"
   secureTextEntry={this.state.showPassword}
/>

              <Icon
              style={styles.eyeIcon}
               name="eye-with-line"
               type="Entypo"
                />

</View>
<View style={styles.row_view}>
<TextInput
   onChangeText={(text)=>this.setState({confirm_password:text})}
  style={styles.flexPadding}
  placeholder="confirm Password"
  placeholderTextColor="#bcbcbc"
  autoCapitalize="none"
   secureTextEntry={this.state.showPassword}
/>

              <Icon
              style={styles.eyeIcon}
               name="eye-with-line"
               type="Entypo"
                />

</View>
          <LinearGradient
          colors={['#CC3A4C', '#E44650', '#E44C4F']}
           start={{x: 0, y: 0}} end={{x: 1, y: 0}}
            style={styles.buttonStyle} onPress={()=> this.signUp()}>
          <Text style={{color:'#ffffff'}}>
              Next
                  </Text>
          </LinearGradient>
              <View
                style={styles.signInView}>
                  <Text
                        style={styles.account_text}>Already have an account ?</Text>
              <TouchableOpacity >
              <Text style={styles.signin_text}>{' '}Sign In</Text>
              </TouchableOpacity>
              </View>
            </View>

          </View>

    );
  }
}
