import React, { Component } from 'react';
import {StyleSheet} from 'react-native';

export default  styles = StyleSheet.create({


    signup_text:
    {
      fontSize:27,
      marginLeft:25,
      marginRight:20,
      marginTop:20,
      padding:10,
      color:'grey'
    },

      label:
      {

      height: 40,
      padding:10,
      borderColor: '#e2e2e2',
      borderRadius:5,
      marginLeft:35,
      marginRight:35,
      marginTop:15,
      borderWidth: 1
      },
      mainview:
      {
        flex:1,
        backgroundColor:'#ffffff',
        justifyContent:'center'
      },
      buttonStyle:
    {
      marginLeft:30,
      marginRight:30,
      marginTop:20,
      marginBottom: 10,
      borderRadius:5,
      color:'#ffffff',
      justifyContent:'center',
      alignItems:'center',
      height:40,
      backgroundColor:'#E44C4F'
    },
    signInView:
      {

       flexDirection:'row' ,
       alignItems:'center',
       justifyContent:'center',
       marginTop:10,
       marginBottom:20,
      },
      searchSection: {
          flex: 1,
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: '#fff',
      },
      eyeIcon: {
        padding:10,
        color:'#bcbcbc',
        fontSize:20
      },
      row_view:{
          marginLeft:35,
          marginRight:35,
          borderColor: '#e2e2e2',
          borderRadius:5,
          borderWidth: 1,
          marginTop:15,
          height:40,
          flexDirection:'row'
      },
      flexPadding:{
        flex:1,
        padding:10

      },
      account_text:{

        fontSize:15,
        color:'#bcbcbc'
      },
      signin_text:{
        textDecorationLine: 'underline',color:'#8198EB'
      }
     });
