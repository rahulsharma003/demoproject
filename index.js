/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import Signup from './Src/Signup/signup'
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => Signup);
